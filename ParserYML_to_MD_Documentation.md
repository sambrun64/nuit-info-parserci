# Documentation - parserYML_MD.sh
> Authors : BottonGS

> Version : 1.0.0v

- [Documentation - parserYML\_MD.sh](#documentation---parseryml_mdsh)
  - [Definition Fonctionelle](#definition-fonctionelle)
    - [Utilisation Classique](#utilisation-classique)
  - [Definition Technique](#definition-technique)
    - [Parser YAML / YML -\> PLAIN TEXT](#parser-yaml--yml---plain-text)
    - [Parser PLAIN TEXT -\> MARDOWN](#parser-plain-text---mardown)

## Definition Fonctionelle

> **Expliquation classique du fonctionnement du script en général et en intégration**

### Utilisation Classique

Ce programme va utiliser le fichier parserYML_MD.sh pour transformer un fichier *.yml et *.yaml en fichier AUTO_GENERATED.md qui contients certaines des informations du fichier de configuration.

Exemple d'utilisation :
```sh
bash parserYML_MD.sh <nom_du_fichier.y(a)ml>
```

- Ce fichier peux etre utiliser en liens avec un gitlab CI/CD.
- En appellant la commande **parserYML_MD.sh** dans le **.gitlab-ci.yml** le dépot sera capable d'autogénerer le fichier MARKDOWN et ce pendant l'intégration (potentiellement a chaque push si configuration adéquat). 
 
## Definition Technique

> **Expliquation technique du script**

### Parser YAML / YML -> PLAIN TEXT 

Notre fichier parse un fichier .yaml et .yml en utilisant les regex bash.

Ces regex sont convertie en ligne de texte facilement modifiable et parsable en utilisant des caractères différent et est envoyé dans un fichier texte temporaire.

### Parser PLAIN TEXT -> MARDOWN

Notre fichier temporaire est alors lu ligne par ligne en séparant par les charactères choisis dans la partie parsage YAML.

Notre script separes les titre 1 du 2 et 3 et permet de créer des liste a puce si certaines information sont a la suite. 