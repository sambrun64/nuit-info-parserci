# stages
- "tests"
# codespell
## stage
> "tests"
## image
## name
> "python:${IMAGE_TAG}"
## variables
## CODESPELL_DIRECTORY
> "."
## CODESPELL_DICTIONARY
> "dictionary.txt"
## CODESPELL_VERSION
> "2.2.1"
## IMAGE_TAG
> "3.10-alpine3.16"
## script
- "| "
- "pip install -q codespell
- "|"
- "|"
- "codespell -c ${DICTIONARY_OPTION} ${IGNORE_FILES_OPTION}"
