# stages
- "environment"
- "build"
- "test"
- "deploy"
- "internal"
- "alpha"
- "beta"
- "production"
## image
> "docker:stable"
## stage
> "environment"
## services
- "docker:dind"
## script
- "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY"
- "docker pull --quiet $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG || true"
- "docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG ."
- "docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
# updateContainer
## extends
> ".updateContainerJob"
## only
## changes
> "Dockerfile"
# ensureContainer
## extends
> ".updateContainerJob"
## allow_failure
> "true"
## before_script
- "mkdir -p ~/.docker && echo '{\"experimental\": \"enabled\"}' > ~/.docker/config.json"
- "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY"
- "|"
## image
> "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
## stage
> "build"
## before_script
- "echo "$signing_jks_file_hex" | xxd -r -p - > android-signing-keystore.jks"
- "export VERSION_CODE
- "export VERSION_SHA
## after_script
- "rm -f android-signing-keystore.jks || true"
## artifacts
## paths
> "app/build/outputs"
# buildDebug
## extends
> ".build_job"
## script
- "bundle exec fastlane buildDebug"
# buildRelease
## extends
> ".build_job"
## script
- "bundle exec fastlane buildRelease"
## environment
## name
> "production"
# testDebug
## image
> "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
## stage
> "test"
## dependencies
- "buildDebug"
## script
- "bundle exec fastlane test"
# publishInternal
## image
> "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
## stage
> "internal"
## dependencies
- "buildRelease"
## when
> "manual"
## before_script
- "echo $google_play_service_account_api_key_json > ~/google_play_api_key.json"
## after_script
- "rm ~/google_play_api_key.json"
## script
- "bundle exec fastlane internal"
## image
> "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
## when
> "manual"
## before_script
- "echo $google_play_service_account_api_key_json > ~/google_play_api_key.json"
## after_script
- "rm ~/google_play_api_key.json"
# promoteAlpha
## extends
> ".promote_job"
## stage
> "alpha"
## script
- "bundle exec fastlane promote_internal_to_alpha"
# promoteBeta
## extends
> ".promote_job"
## stage
> "beta"
## script
- "bundle exec fastlane promote_alpha_to_beta"
# promoteProduction
## extends
> ".promote_job"
## stage
> "production"
## only
## variables
> "$CI_COMMIT_BRANCH 
## script
- "bundle exec fastlane promote_beta_to_production"
