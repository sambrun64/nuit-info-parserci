# variables
## EXE_RELEASE_FOLDER
> "YourApp\bin\Release"
## MSI_RELEASE_FOLDER
> "Setup\bin\Release"
## TEST_FOLDER
> "Tests\bin\Release"
## DEPLOY_FOLDER
> "P:\Projects\YourApp\Builds"
## NUGET_PATH
> "C:\NuGet\nuget.exe"
## MSBUILD_PATH
> "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe"
## NUNIT_PATH
> "C:\Program Files (x86)\NUnit.org\nunit-console\nunit3-console.exe"
# stages
- "build"
- "test"
- "deploy"
# build_job
## stage
> "build"
## only
- "tags # the build process will only be started by git tag commits"
## script
- "'& "$env:NUGET_PATH" restore' # restore Nuget dependencies"
- "'& "$env:MSBUILD_PATH" /p:Configuration
## artifacts
## expire_in
> "1 week # save gitlab server space, we copy the files we need to deploy folder later on"
## paths
> "'$env:EXE_RELEASE_FOLDER\YourApp.exe' # saving exe to copy to deploy folder"
## paths
> "'$env:MSI_RELEASE_FOLDER\YourApp Setup.msi' # saving msi to copy to deploy folder"
## paths
> "'$env:TEST_FOLDER\' # saving entire Test project so NUnit can run tests"
# test_job
## stage
> "test"
## only
- "tags"
## script
- "'& "$env:NUNIT_PATH" ".\$env:TEST_FOLDER\Tests.dll"' # running NUnit tests"
## artifacts
## when
> "always # save test results even when the task fails"
## expire_in
> "1 week # save gitlab server space, we copy the files we need to deploy folder later on"
## paths
> "'.\TestResult.xml' # saving NUnit results to copy to deploy folder"
## dependencies
- "build_job"
# deploy_job
## stage
> "deploy"
## only
- "tags"
## script
- "$commitSubject 
- "$deployFolder 
- "xcopy /y ".\$env:EXE_RELEASE_FOLDER\YourApp.exe" "$deployFolder""
- "xcopy /y ".\$env:MSI_RELEASE_FOLDER\YourApp Setup.msi" "$deployFolder""
- "xcopy /y ".\TestResult.xml" "$deployFolder""
## dependencies
- "build_job"
- "test_job"
## environment
> "production"
