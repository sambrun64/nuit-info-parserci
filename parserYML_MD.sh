#!/bin/bash

outFile="AUTO_GENERATED_5.md"

# source: https://github.com/mrbaseman/parse_yaml.git
# Résumer : Fonction qui va recevoir un fichier en parametre
# et va retouner un version texte d'un fichier y(a)ml parsable
# Entrée : Un fichier yml ou yaml
# Sortie : Le texte du yaml en version texte parsable
function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|,$s\]$s\$|]|" \
        -e "s|^\($s\)\($w\)$s:$s\[$s\(.*\)$s\]|\1\2:\n\1  - \3|;p" $1 | \
   sed -ne "s|,$s}$s\$|}|" \
        -e    "s|^\($s\)-$s{$s\(.*\)$s}|\1-\n\1  \2|;p" | \
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)-$s[\"']\(.*\)[\"']$s\$|\1$fs$fs\2|p" \
        -e "s|^\($s\)-$s\(.*\)$s\$|\1$fs$fs\2|p" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p" | \
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]; idx[i]=0}}
      if(length($2)== 0){  vname[indent]= ++idx[indent] };
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) { vn=(vn)(vname[i])("|")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, vname[indent], $3);
      }
   }'
}

# Si il n'y a pas de fichier en argument on coupe l'application
if [ -z "$1" ]
then
   echo "Missing File !"
   exit 0
fi

# On prend la sortie texte de la fonction et on la redirige
# Dans un fichier texte temporaire pour son parsage ligne par ligne
parse_yaml $1 > tmp.txt 

# On supprime le fichier si il existe afin de le mettre a jour
if [ -e "$outFile" ]
then
    rm "$outFile"
fi

# Variable temporaire
input="tmp.txt"
P1prec=""
P2prec=""

# Variable qui définie tous les chiffres
re='^[0-9]+$'

# On va ligne chaque ligne de plain text les une après les autre
while IFS= read -r line
do
   skip=false
   
   # On coupe les ligne en deux
   VAR1=$(echo $line | cut -f1 -d=)
   VAR2=$(echo $line | cut -f2 -d=)
    
   # On recupere les informations
   P1=$(echo $VAR1 | cut -f1 -d "|")
   P2=$(echo $VAR1 | cut -f2 -d "|")
   P3=$(echo $VAR1 | cut -f3 -d "|")
   
   # Si il y avais deja la meme balise on ne l'a réecrit pas 
   if [[ ! "$P1" == "$P1prec" ]]
   then  
        echo "# $P1" >> "$outFile"
    fi

    # Si il y avais deja la meme balise on ne l'a réecrit pas
    # Et si elle n'est pas vide
    if [ ! -z "$P2" ] && [[ ! "$P2" == "$P2prec" ]] 
    then
    	# Si c'est un chiffre
        if ! [[ $P2 =~ $re ]]
        then
            echo "## $P2" >> "$outFile"
        else
            echo "- $VAR2" >> "$outFile"
            skip=true
        fi
      
    fi

    # Et si elle n'est pas vide	
    if [ ! -z "$P3" ]
    then
    	# Si c'est un chiffre
        if ! [[ $P3 =~ $re ]]
        then
            echo "## $P3" >> "$outFile"
        else
            echo "- $VAR2" >> "$outFile"
            skip=true
        fi
    fi

    # Et si elle n'est pas vide	
    if [[ ! -z "$VAR2" ]] && ! $skip
    then
      echo "> $VAR2" >> "$outFile"
    fi

    P1prec=$P1
    P2prec=$P2

done < "$input"

# On supprimme le fichier temporaire
rm "tmp.txt"
